from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm

# Create your views here.


def todo_list_list(request):
    todos = TodoList.objects.all()
    context = {"todo_list_list": todos}
    return render(request, "todos/lists.html", context)


def todo_list_detail(request, id):
    list = get_object_or_404(TodoList, id=id)
    context = {
        "todos_detail": list,
    }
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoListForm()

    context = {
        "form": form,
    }

    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    list = get_object_or_404(TodoList, id=id)

    if request.method == "POST":
        form = TodoListForm(request.POST, instance=list)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoListForm(instance=list)
    context = {
        "form": form,
        "todo_detail": list,
    }
    return render(request, "todos/modify.html", context)


def todo_list_delete(request, id):
    list = get_object_or_404(TodoList, id=id)

    if request.method == "POST":
        list.delete()
        return redirect("todo_list_list")

    context = {
        "todo_delete": list,
    }
    return render(request, "todos/delete.html", context)


def todo_item_create(request):
    # todos = TodoList.objects.get(id=id)

    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            todos = form.save()
            return redirect("todo_list_detail", id=todos.list.id)
    else:
        form = TodoItemForm()
    context = {
        "form": form,
    }
    return render(request, "todos/newtask.html", context)
